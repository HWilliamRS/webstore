<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\ProductCategory;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$products = Product::where('active', 1)->get();*/
        $products = DB::table('products')
                        ->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
                        ->select('products.id', 'products.name', 'products.description', 
                                 'unit_price', 'units_in_stock', 'product_categories.name as categoryName')
                         ->where('products.active', '1')
                            ->get();
        return view('manage.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productCategories = ProductCategory::select('id', 'name')
                                        ->where('active', 1)
                                        ->get();

        $categories = [];
        $i = 0;

        foreach ($productCategories as $productCategory) {
            $categories[$i] = $productCategory->id . ' - ' . $productCategory->name;
            $i++;
        }

        return view('manage.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateFields($request);

        $product = new Product();

        $idCategory = $request->product_category_id;

        $product->name = $request->name;
        $product->description = $request->description;
        $product->unit_price = $request->unit_price;
        $product->units_in_stock = $request->units_in_stock;
        $product->product_category_id = $idCategory;

        $product->save();

        request()->session()->flash("flash_message", "¡Producto creado con éxito!");

        return redirect('/manage/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where('id', $id)->first();

        return view('manage/products/show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {

        $productCategories = ProductCategory::select('id', 'name')
                                                ->where('active', 1)
                                                ->get();

        $index = array_search( $product->product_category_id, array_column($productCategories->toArray(), 'id') );

        $categories = [];
        $i = 0;

        foreach ($productCategories as $productCategory) {
            $categories[$i] = $productCategory->id . ' - ' . $productCategory->name;
            $i++;
        }

        return view('manage/products/edit', compact('product', 'categories', 'index'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->validateFields($request);

        $idCategory = $this->getCategoryId($request->product_category_id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->unit_price = $request->unit_price;
        $product->units_in_stock = $request->units_in_stock;
        $product->product_category_id = $idCategory;

        $product->save();

        request()->session()->flash("flash_message", "¡Producto actualizado con éxito!");

        return redirect('/manage/products');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::where('id', $id)->first();

        $product->active = 0;

        $product->save();

        request()->session()->flash("flash_message", "¡Producto borrado con éxito!");

        return redirect('/manage/products');
    }

    public function getCategoryId($index){
        $categories = session('categories');
        $categories = $categories[$index];
        $idCategory = substr($categories, 0, 2);
        $idCategory = str_replace(' ', '', $idCategory);

        return $idCategory;
    }

    public function validateFields(Request $request){

        $validatedData = $request->validate(
            [
                'name' => 'required',
                'description' => 'required',
                'unit_price' => 'required|numeric',
                'units_in_stock' => 'required|numeric',
                'product_category_id' => 'required'
            ],
            [
                'name.required' => 'El campo nombre es requerido',
                'description.required' => 'El campo descripción es requerido',
                'unit_price.required' => 'El campo Precio Unitario es requerido',
                'unit_price.numeric' => 'El campo Precio Unitario solo permite numeros',
                'units_in_stock.required' => 'Agregar unidades en Stock',
                'units_in_stock.numeric' => 'El campo Stock solo permite numeros',
                'product_category_id.required' => 'El campo Categoría de Producto es requerido'
            ]
        );

        return $validatedData;
    }
}