<?php

namespace App\Http\Controllers;

use App\people;

use Illuminate\Http\Request;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peoples = people::active()->get();
        $model_text =  trans('models.people') ;
        $model = 'people';
        return view("manage.people.index", compact('peoples','model_text', 'model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.people.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate(
            [
                'name' => 'required|min:3|max:45',
                'Lastname' => 'required|min:8|max:45',
                'email' => 'required|email',
                'Address'=>'required|min:15|max:45',
                'phone_number'=>'required|min:12|max:13',
                'date_of_birth'=>'required|min:10|max:12',
                'Bank_Card'=>'required|min:16|max:19'
            ],
            [
                'name.required' => 'Por favor introduzca un nombre.',
                'name.min' => 'El nombre debe de contener al menos 3 carcteres.',
                'name.max' => 'El nombre no puede pasar de 45 caracteres.',
                
                'Lastname.required' => 'Por favor introduzca un apellido.',
                'Lastname.min' => 'El apellido debe de contener al menos 8 carcteres.',
                'Lastname.max' => 'El apellido no puede pasar de 45 caracteres.',
                
                'email.required' => 'Por favor introduzca su correo electrónico.',
                'email.unique' => 'El correo electrónico introducido ya está siendo utilizado.',
                'email.email' => 'Por favor introduzca un correo electrónico válido.',

                'Address.required' => 'Por favor introduzca una direccion.',
                'Address.unique' => 'La direccion introducida ya está siendo utilizada.',
                'Address.min' => 'La direccion debe de contener al menos 15 carcteres.',
                'Address.max' => 'La direccion no puede pasar de 45 caracteres.',

                'phone_number.required' => 'Por favor introduzca un numero telefonico.',
                'phone_number.unique' => 'El numero telefonico introducido ya está siendo utilizado.',
                'phone_number.min' => 'El numero telefonico debe de contener al menos 12 carcteres.',
                'phone_number.max' => 'El numero telefonico no puede pasar de 13 caracteres.',

                'date_of_birth.required' => 'Por favor introduzca un apellido.',
                'date_of_birth.min' => 'La fecha de nacimiento debe de contener al menos 10 carcteres.',
                'date_of_birth.max' => 'La fecha de nacimiento no puede pasar de 12 caracteres.',

                'Bank_Card.required' => 'Por favor introduzca un apellido.',
                'Bank_Card.unique' => 'Su tarjeta bancaria ya está siendo utilizado.',
                'Bank_Card.min' => 'Su tarjeta bancaria debe de contener al menos 16 carcteres.',
                'Bank_Card.max' => 'Su tarjeta bancaria no puede pasar de 19 caracteres.'

                
            ]
        );


        $people = new people($request->all());

        

        $people->save();

        $request->session()->flash("flash_message","El registro fue creado de manera satisfactoria!");

        return redirect('/manage/people');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\people  $people
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        $Peoples=people::findOrfail($id);
        return view('manage.people.read',compact('Peoples'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\people  $people
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
         $peoples=people::findOrfail($id);
        
        return view('manage.people.edit',compact('peoples'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\people  $people
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $peoples)
    {
        $people=request()->except('_token','_method');

        people::where('id','=',$peoples)->update( $people);

        $request->session()->flash("flash_message","El usuario fue actualizado de manera satisfactoria!");

        return redirect('/manage/people');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\people  $people
     * @return \Illuminate\Http\Response
     */
    public function destroy( $people)
    {
        \DB::table('people')
        ->updateOrInsert(
            ['id' => $people],
            ['active' => '0']
        );
       

        request()->session()->flash("flash_message","El usuario fue eliminado de manera satisfactoria!");

        return redirect('/manage/people');
    }

    public function validateFields(Request $request){
        $validatedData = $request->validate(
            [
                'name' => 'required|min:3|max:45',
                'Lastname' => 'required|min:8|max:45',
                'email' => 'required|email',
                'Address'=>'required|min:15|max:45',
                'phone_number'=>'required|min:12|max:13',
                'date_of_birth'=>'required|min:10|max:12',
                'Bank_Card'=>'required|min:16|max:19'
            ],
            [
                'name.required' => 'Por favor introduzca un nombre.',
                'name.min' => 'El nombre debe de contener al menos 3 carcteres.',
                'name.max' => 'El nombre no puede pasar de 45 caracteres.',
                
                'Lastname.required' => 'Por favor introduzca un apellido.',
                'Lastname.min' => 'El apellido debe de contener al menos 8 carcteres.',
                'Lastname.max' => 'El apellido no puede pasar de 45 caracteres.',
                
                'email.required' => 'Por favor introduzca su correo electrónico.',
                'email.unique' => 'El correo electrónico introducido ya está siendo utilizado.',
                'email.email' => 'Por favor introduzca un correo electrónico válido.',

                'Address.required' => 'Por favor introduzca una direccion.',
                'Address.unique' => 'La direccion introducida ya está siendo utilizada.',
                'Address.min' => 'La direccion debe de contener al menos 15 carcteres.',
                'Address.max' => 'La direccion no puede pasar de 45 caracteres.',

                'phone_number.required' => 'Por favor introduzca un numero telefonico.',
                'phone_number.unique' => 'El numero telefonico introducido ya está siendo utilizado.',
                'phone_number.min' => 'El numero telefonico debe de contener al menos 12 carcteres.',
                'phone_number.max' => 'El numero telefonico no puede pasar de 13 caracteres.',

                'date_of_birth.required' => 'Por favor introduzca un apellido.',
                'date_of_birth.min' => 'La fecha de nacimiento debe de contener al menos 10 carcteres.',
                'date_of_birth.max' => 'La fecha de nacimiento no puede pasar de 12 caracteres.',

                'Bank_Card.required' => 'Por favor introduzca un apellido.',
                'Bank_Card.unique' => 'Su tarjeta bancaria ya está siendo utilizado.',
                'Bank_Card.min' => 'Su tarjeta bancaria debe de contener al menos 16 carcteres.',
                'Bank_Card.max' => 'Su tarjeta bancaria no puede pasar de 19 caracteres.'

                
            ]
        );

        return $validatedData;
    }
}
