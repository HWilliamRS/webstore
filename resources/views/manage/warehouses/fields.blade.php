<div class="form-group">
    {!! Form::label('name_code', trans('models.name_code').': ') !!}
    {!! Form::text('name_code',null,['class'=>'form-control', 'placeholder'=>'Introducir código']) !!}
</div>

<div class="form-group">
    {!! Form::label('description',trans('models.description').': ') !!}
    {!! Form::text('description',null,['class'=>'form-control', 'placeholder'=>'Introducir descripción']) !!}
</div>

<div class="form-group">
    {!! Form::label('location',trans('models.location').': ') !!}
    {!! Form::text('location',null,['class'=>'form-control', 'placeholder'=>'Introducir ubicación']) !!}
</div>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
    $.validator.setDefaults({
        submitHandler: function () {
            $("#productCategories").submit();
        }
    });
    $('#productCategories').validate({
        rules: {
            name_code: {
                required: true
            },
            description: {
                required: true
            },
            location: {
                required: true
            }
        },
        messages: {
            name_code: {
                required: "Por favor introduzca el código del campo."
            },
            description: {
                required: "Por favor introduzca la descripción del campo."
            },
            location: {
                required: "Por favor introduzca la ubicación del campo."
            },

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
});
</script>
