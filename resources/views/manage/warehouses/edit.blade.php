@extends('layouts.admin')


@section('main_content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- jquery validation -->

            <div class="card card-primary mt-4 ">
                <div class="card-header mb-4">
                    <h3 class="card-title">{{ trans('messages.edit') }} {{ trans('models.warehouse') }}</h3>
                </div>
            @include('errors.list')
            <!-- /.card-header -->
                <!-- form start -->

                {!! Form::model($warehouse, ['method'=>'PATCH','action' => ['WarehousesController@update', $warehouse->id],'class '=> 'row',
                'id'=>'warehouses' , 'name' => 'warehouses' , 'warehouse' => 'form']) !!}
                <div class="container-fluid">
                    <div class="card-body">
                        @method('PATCH')
                        @csrf

                        @include('manage.warehouses.fields')

                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right m-3">Guardar</button>
                        <a href="/manage/warehouses" class="btn btn-default float-right m-3">Cancelar</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.card -->
        </div>
        <!--/.col (left) -->

    </div>
    <!-- /.row -->

@endsection('main_content');
