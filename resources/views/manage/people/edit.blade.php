@extends('layouts.admin')


@section('main_content')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->

        <div class="card card-primary mt-4 ">
            <div class="card-header mb-4">
                <h3 class="card-title">Actualizar Persona</h3>
            </div>
            @include('errors.list')
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="people" name="people" method="post" action="{{ url('/manage/people/' . $peoples->id) }}">
                <div class="card-body">
                    @method('PATCH')
                    @csrf

                    @include('manage.people.fields')
                    
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right m-3">Guardar</button>
                    <button type="button"  class="btn btn-default float-right m-3">Cancelar</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">

    </div>
    <!--/.col (right) -->
</div>
<!-- /.row -->



@endsection