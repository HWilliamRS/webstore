@extends('layouts.admin')

@section('main_content')
    <div class="container d-flex justify-content-between">
        <div>
            <h1>Personas</h1>
        </div>
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="users_table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Datos</th>
                            <th>Informacion</th>
                        </tr>
                        </thead>

                        <tbody>
                            
                       
                            <tr>
                               
                                <td>  ID </td>
                                <td> {{ $Peoples->id }} </td>
                            </tr>
                            <tr>
                               
                                <td>  Nombre y apellido </td>
                                <td> {{ $Peoples->name }}  {{ $Peoples->lastname }} </td>
                                
                            </tr>
                            <tr>
                               
                                <td>  Correo Electronico </td>
                                <td> {{ $Peoples->email }}   </td>
                                
                            </tr>
                            <tr>
                               
                                <td>  Direccion</td>
                                <td> {{ $Peoples->Address }}   </td>
                                
                            </tr>
                            <tr>
                               
                                <td> Numero telefonico</td>
                                <td> {{ $Peoples->phone_number }}   </td>
                                
                            </tr>
                            <tr>
                               
                                <td>  Fecha de nacimiento </td>
                                <td> {{ $Peoples->date_of_birth }}  </td>
                                
                            </tr>
                            <tr>
                               
                                <td>  Tarjeta de credito </td>
                                <td> {{ $Peoples->Bank_Card }} </td>
                                
                            </tr>

                            <tr>
                               
                                <td> Estado </td>
                                @if($Peoples->active ==1 )
                                <td>Activo   </td>
                                @else
                                <td>No activo</td>
                                @endif
                            </tr>
                           

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection