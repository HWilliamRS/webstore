<div class="form-group">
    {!! Form::label('name', 'Nombre: ') !!}
    {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Introducir nombre']) !!}
</div>

<div class="form-group">
    {!! Form::label('description','Descripción: ') !!}
    {!! Form::text('description',null,['class'=>'form-control', 'placeholder'=>'Introducir descripción']) !!}
</div>

<div class="form-group">
    {!! Form::label('unit_price','Precio Unitario: ') !!}
    {!! Form::number('unit_price',null,['class'=>'form-control', 'placeholder'=>'Introducir precio unitario', 'step' => 'any']) !!}
</div>

<div class="form-group">
    {!! Form::label('units_in_stock','Unidades en Stock: ') !!}
    {!! Form::number('units_in_stock',null,['class'=>'form-control', 'placeholder'=>'Introducir unidades en Stock']) !!}
</div>

@if (strpos(Request::url(), 'create'))
    @php
        $index = 'S'
    @endphp
@endif

<div class="form-group">
    {!! Form::label('productCategorie','Categoría: ') !!}
    {!! Form::select('product_category_id', $categories, $index, ['class'=>'form-control', 'placeholder'=>'Introducir Categoría del Producto']) !!}
    
    {{ session()->flash('categories', $categories) }}
</div>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
    $.validator.setDefaults({
        submitHandler: function () {
            $("#productCategories").submit();
        }
    });
    $('#productCategories').validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true
            },
            slug: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Por favor introduzca el nombre del campo categoría producto."
            },
            description: {
                required: "Por favor introduzca la descripción del campo categoría producto."
            },
            slug: {
                required: "Por favor introduzca el término del campo categoría producto."
            },

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
});
</script>
