@extends('layouts.admin')


@section('main_content')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->

        <div class="card card-primary mt-4 ">
            <div class="card-header mb-4">
                <h3 class="card-title">Crear usuario</h3>
            </div>
            <div >
              @foreach ($errors->all() as $error)
              <div class="alert alert-danger alert-dismissible fade show" role="alert" >
                <strong>Error!</strong> {{ $error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endforeach
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="users" name="users" method="post" action="{{ url('/manage/users') }}">
                <div class="card-body">
                    @method('POST')
                    @csrf

                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="name" name="name" id="name" class="form-control"  placeholder="Introducir nombre"
                            value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Correo electrónico</label>
                        <input type="email" name="email" id="email" class="form-control" 
                            placeholder="Introducir correo electrónico" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input type="password" name="password" id="password" class="form-control"
                            placeholder="Introducir contraseña">
                    </div>
                    <div class="form-group">
                        <label for="password1">Confirmar contraseña</label>
                        <input type="password" name="password1" id="password1" class="form-control"
                            placeholder="Confirmar contraseña">
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right m-3">Crear</button>
                    <button type="reset" class="btn btn-default float-right m-3">Cancelar</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">

    </div>
    <!--/.col (right) -->
</div>
<!-- /.row -->

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function() {
      $.validator.setDefaults({
        submitHandler: function () {
          $("#users").submit();
        }
      });
      $('#users').validate({
        rules: {
          name: {
            required: true
          },
          email: {
            required: true,
            email: true,
          },
          password: {
            required: true,
            minlength: 8,
            maxlength:45
          },
          password1: {
            required: true,
            equalTo: "#password"
          },

        },
        messages: {
          name: {
            required: "Por favor introduzca un nombre."
          },
          email: {
            required: "Por favor introduzca su correo electrónico.",
            email: "Por favor introduzca un correo electrónico válido."
          },
          password: {
            required: "Por favor introduzca una contraseña",
            minlength: "Su contraseña debe de tener al menos 8 caracteres.",
            maxlength: "Su contraseña no debe contener mas de 45 caracteres."
          },
          password1: {
            required: "Por favor debe repetir la contraseña.",
            equalTo: "Las contraseñas introducidas no coinciden."
          },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });
    });
</script>

@endsection