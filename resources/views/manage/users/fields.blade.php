<div class="form-group">
    <label for="name">Nombre</label>
    <input type="name" name="name" id="name" class="form-control"  placeholder="Introducir nombre"
        value="{{ old('name') }}">
</div>
<div class="form-group">
    <label for="email">Correo electrónico</label>
    <input type="email" name="email" id="email" class="form-control" 
        placeholder="Introducir correo electrónico" value="{{ old('email') }}">
</div>
<div class="form-group">
    <label for="password">Contraseña</label>
    <input type="password" name="password" id="password" class="form-control"
        placeholder="Introducir contraseña">
</div>
<div class="form-group">
    <label for="password1">Confirmar contraseña</label>
    <input type="password" name="password1" id="password1" class="form-control"
        placeholder="Confirmar contraseña">
</div>