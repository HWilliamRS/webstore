@extends('layouts.admin')

@section('main_content')
    <div class="container d-flex justify-content-between">
        <div>
            <h1>{{$model_text}}s</h1>
        </div>
        <div class="d-flex align-items-center">
            <a href="{{ action('ProductCategories' . 'Controller@create') }}" class="btn btn-block btn-primary">Crear {{   Illuminate\Support\Str::lower($model_text)  }}</a>
        </div>
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                @include('partials.flash')
                <div class="card-body">
                    <table id="product_categories_table" class="datatable table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Término</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($productCategories as $productCategory)
                            <tr>
                                <td>{{ $productCategory->id }}</td>
                                <td>{{ $productCategory->name }}</td>
                                <td>{{ $productCategory->description}} </td>
                                <td>{{ $productCategory->slug}} </td>
                                <td>
                                    <div class="d-flex">
                                        <ul class="list-inline center mx-auto justify-content-center m-0">
                                            <li class="list-inline-item">
                                                <a class="nav-link" href="{{ url('/manage/productCategories/' . $productCategory->id ) }}"
                                                   productCategory="button"><i class="fas fa-book-open"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="nav-link"
                                                   href="{{ url('/manage/productCategories/' . $productCategory->id ) . '/edit' }}"
                                                   productCategory="button"><i class="fas fa-edit"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="nav-link" href="#" productCategory="button"
                                                   onclick="deleteModelRecord({{ $productCategory->id }} , 'productCategories') "><i
                                                        class="fas fa-trash-alt"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('partials.delete_dialog')
    <script>
    document.addEventListener("DOMContentLoaded", function () {

        $('.datatable').DataTable({
            "responsive": true,
            "autoWidth": false,
        });

    });
    </script>

@endsection
