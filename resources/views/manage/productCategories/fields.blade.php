<div class="form-group">
    {!! Form::label('name', trans('models.name').': ') !!}
    {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Introducir nombre']) !!}
</div>

<div class="form-group">
    {!! Form::label('description',trans('models.description').': ') !!}
    {!! Form::text('description',null,['class'=>'form-control', 'placeholder'=>'Introducir descripción']) !!}
</div>

<div class="form-group">
    {!! Form::label('slug',trans('models.slug').': ') !!}
    {!! Form::text('slug',null,['class'=>'form-control', 'placeholder'=>'Introducir término']) !!}
</div>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
    $.validator.setDefaults({
        submitHandler: function () {
            $("#productCategories").submit();
        }
    });
    $('#productCategories').validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true
            },
            slug: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Por favor introduzca el nombre del campo categoría producto."
            },
            description: {
                required: "Por favor introduzca la descripción del campo categoría producto."
            },
            slug: {
                required: "Por favor introduzca el término del campo categoría producto."
            },

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
});
</script>
