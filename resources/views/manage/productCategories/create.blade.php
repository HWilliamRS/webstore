@extends('layouts.admin')


@section('main_content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- jquery validation -->

            <div class="card card-primary mt-4 ">
                <div class="card-header mb-4">
                    <h3 class="card-title">Crear categoría producto</h3>
                </div>
            @include('errors.list')
            <!-- /.card-header -->
                <!-- form start -->

                {!! Form::model($productCategory = new \App\Models\ProductCategory, ['action' => 'ProductCategoriesController@store','class '=> 'row',
                'id'=>'productCategories' , 'name' => 'productCategories' , 'productCategory' => 'form']) !!}
                <div class="container-fluid">
                    <div class="card-body">
                        @method('POST')
                        @csrf

                        @include('manage.productCategories.fields')

                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right m-3">Crear</button>
                        <button type="reset" class="btn btn-default float-right m-3">Cancelar</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.card -->
        </div>
        <!--/.col (left) -->

    </div>
    <!-- /.row -->

@endsection('main_content');
