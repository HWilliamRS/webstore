<?php

return [

   // Dashboard admin
   'dashboard' => 'Escritorio',
   'metaintitutions' => 'Instituciones meta',
   'othersinstitutions' => 'Otras instituciones',
   'executive_resume' => 'Resumen ejecutivo',
   'institutional_memory' => 'Memoria institucional',


   // Dashboard cliente
   'documents' => 'Documentos',


   // Users
   'create_user' => 'Crear usuario',
   'users' => 'Usuarios',
   'edit_user' => 'Editar usuario',
   'user_profile' => 'Perfil de usuario',


   // Common
   'id' => 'ID',
   'email' => 'Correo electrónico',
   'name' => 'Nombre',
   'description' => 'Descripción',
   'processed' => 'Procesadas',
   'slug' => 'Frase corta',
   'name_code' => 'Código',
   'location' => 'Ubicación',


   // User
   'user' => 'Usuario',
   'username' => 'Nombre de usuario',
   'password' => 'Contraseña',
   'password_confirmation' => 'Confirmación de contraseña',
   'remenber_password' => 'Recordar contraseña',
   'update_user_success' =>  'Los datos del usurio fueron actualizados correctamente!',
   'invalid_credentials' => 'Credenciales invalidas',


   // UserDetails
   'firstname' => 'Nombre',
   'lastname'  => 'Apellido',
   'phone_ext' => 'Ext.',
   'mobile' => 'Celular',
   'update_userDetail_success' => 'Los datos personales del usuario fueron actualizados correctamente!',


   //Document Type
   'documentType' => 'Tipo de documento',

   //Role
   'role' => 'Rol',
   'roles' => 'Roles',
   'choose_role' => 'Elige un rol',


   //Permissions
   'permission' => 'Permiso',
   'permissions' => 'Permisos',


   //Reports
   'reports' => 'Reportes',
   'filter' => 'Filtrar',


    //ProductCategory
    'product_category' => "Categoría producto",
    'product_categories' => "Categorías producto",

    //Warehouse
    'warehouse' => "Almacén",
    'warehouses' => "Almacenes",

];
