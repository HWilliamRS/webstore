<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\people;
use Faker\Generator as Faker;

$factory->define(people::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'lastname'=>$faker->lastName,
        'email'=>$faker->unique()->safeEmail,
        'Address'=>$faker->address,
        'phone_number'=>$faker->phoneNumber,
        'date_of_birth'=>$faker->date('Y-m-d', 'now'),
        'Bank_Card'=>$faker->creditCardNumber(null,true)

    ];
});
