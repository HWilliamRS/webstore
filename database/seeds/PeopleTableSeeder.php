<?php

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            'id'=>1,
            'name'=>'Juan',
            'lastname'=>'Perez',
            'email'=>'jp@hotmail.com',
            'Address'=>'Santo Domingo D.N',
            'phone_number'=>'809-222-1111',
            'date_of_birth'=>'2000-02-03',
            'Bank_Card'=>'1122-3333-4444-5555',


        ]);
        $people=factory(App\people::class, 10)->create();
    }
}
