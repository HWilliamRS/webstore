<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'slug' => 'sysadmin',
            'name' => 'Administrador técnico del sistema.',
            'description' => 'Encargado de todos los mantenimientos internos del sistema.',
            'active' => 1,
        ]);

        DB::table('roles')->insert([
            'slug' => 'webadmin',
            'name' => 'Administrado Web',
            'description' => 'Encargado de los mantenimientos generales del sistema.',
            'active' => 1,
        ]);

        DB::table('roles')->insert([
            'slug' => 'secadmin',
            'name' => 'Administrador de sección',
            'description' => 'Encargado de la mantenimiento y moderación de contenido en secciones específicas.',
            'active' => 1,
        ]);

        DB::table('roles')->insert([
            'slug' => 'storeadmin',
            'name' => 'Administrador o propietario de tienda',
            'description' => 'Usuarios destinados a la gestión de tienda registrada en el sistema',
            'active' => 1,
        ]);

        DB::table('roles')->insert([
            'slug' => 'client',
            'name' => 'Usuario básico de cliente final',
            'description' => 'Usuarios destinados a la utilización del sistema final.',
            'active' => 1,
        ]);
        
    }
}
