<?php

use Illuminate\Database\Seeder;

class WarehousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('warehouses')->insert([
            'name_code' => '001',
            'description' => 'Almacén principal.',
            'location' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit, placerat egestas eget ad vitae cursus.',
            'active' => 1,
        ]);

        DB::table('warehouses')->insert([
            'name_code' => '002',
            'description' => 'Almacén Santiago.',
            'location' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit, placerat egestas eget ad vitae cursus.',
            'active' => 1,
        ]);

        DB::table('warehouses')->insert([
            'name_code' => '003',
            'description' => 'Almacén La Romana.',
            'location' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit, placerat egestas eget ad vitae cursus.',
            'active' => 1,
        ]);
    }
}
